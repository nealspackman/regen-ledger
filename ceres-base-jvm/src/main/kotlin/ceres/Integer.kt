package ceres.ast

import java.math.BigDecimal
import java.math.BigInteger

actual typealias Integer = BigInteger

actual typealias Real = BigDecimal